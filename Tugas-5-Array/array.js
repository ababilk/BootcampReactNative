//no. 1
function range(startNum, finishNum){
    if(startNum == undefined || finishNum == undefined){
        return -1
    }else{
        var hasil = []
        if(startNum<finishNum){
        //console.log("masuk if")
            for (let index = startNum; index <= finishNum; index++) {
                //console.log("for"+index)
                hasil.push(index)
            }
        }else{
            for (let index = startNum; index >= finishNum; index--) {
                hasil.push(index);
         }
        }
        return hasil
    }
}
console.log("No. 1")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//no. 2
function rangeWithStep(startNum, finishNum, step) {
    var hasil =[];
    if(startNum<finishNum){
        for (let index = startNum; index <= finishNum; index += step) {
            hasil.push(index);
        }
    }else{
        for (let index = startNum; index >= finishNum; index -= step) {
            hasil.push(index)
        }
    }
    return hasil
}
console.log("No.2")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//no.3
function sum(awal, akhir, jarak = 1){
    if(akhir == undefined){
        if(awal == undefined){
            return 0
        }else{
            return awal
        }
    }else{
        var array = rangeWithStep(awal, akhir, jarak);
        var hasil = 0
        array.forEach(element => {
            hasil += element
        });
        return hasil
    }
    
    
}
console.log("No. 3")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//no.4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input){
    input.forEach(element => {
        console.log("Nomor ID: " + element[0])
        console.log("Nama Lengkap: " + element[1])
        console.log("TTL: " + element[2] + " " + element[3])
        console.log("Hobi: " + element[4])
        console.log("")
    });
}

dataHandling(input)

//no. 5
function balikKata(kata) {
    var char = kata.length - 1
    var hasil = ""
    for (let index = char; index >= 0; index--) {
        //console.log(kata.charAt(index))
        hasil +=kata.charAt(index)
    }
    return hasil
}
console.log("No. 5")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//no. 6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
function dataHandling2(input) {
    var inputAwal = input.slice()
    input.splice(1,1,input[1]+" Elsharawy")
    input.splice(2,1,"Provinsi "+input[2])
    input.splice(4,1,"Pria")
    input.push("SMA Internasional Metro")
    console.log(input)//1
    var tanggal = input[3]
    var arrayTanggal = tanggal.split("/");
    var bulan = "";
    switch (arrayTanggal[1]) {
        case '01':
            bulan="Januari"
            break;
        case '02':
            bulan="Februari"
            break;
        case '03':
            bulan="Maret"
            break;
        case '04':
            bulan="April"
            break;
        case '05':
            bulan="Mei"
            break;
        case '06':
            bulan="Juni"
            break;
        case '07':
            bulan="Juli"
            break;
        case '08':
            bulan="Agustus"
            break;
        case '09':
            bulan="September"
            break;
        case '10':
            bulan="Oktober"
            break;
        case '11':
            bulan="November"
            break;
        case '12':
            bulan="Desember"
            break;
        default:
            break;
    }
    console.log(bulan)//2
    var formatTanggal = []
    formatTanggal.push(arrayTanggal[2])
    formatTanggal.push(arrayTanggal[0])
    formatTanggal.push(arrayTanggal[1])
    console.log(formatTanggal)//3
    console.log(arrayTanggal[0] + "-" + arrayTanggal[1] + "-" + arrayTanggal[2])//4
    console.log(inputAwal[1])//5
}

dataHandling2(input)
var tanggal = input[3]
//console.log(tanggal)
var arrayOfStrings = tanggal.split("/");
//console.log(arrayOfStrings)
//console.log(arrayOfStrings[1])
if(arrayOfStrings[1]=='05'){
    //console.log("jan")
}
