//No.1
function arrayToObject(arr) {
    var getDate = new Date();
    var getYear = getDate.getFullYear();
    var umur = 0;
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        umur =getYear - element[3];
        if(element[3]>=getYear || element[3] == undefined){
            umur = "Invalid birth year";
        }else{
            umur = getYear - element[3];
        }
        console.log(index+1 +". "+element[0]+" "+element[1]
        +" : { firstName: \""+element[0]
        +"\", lastName: \""+element[1]
        +"\", gender: \""+element[2]
        +"\", age: "+umur+"}");
        
    }
}
//console.log("soal no.1")
var input = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
//arrayToObject(input);
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
//arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
//arrayToObject(people2) 

//No.2
function shoppingTime(memberId, money) {
    var barang = [["Sepatu Stacattu",1500000],["Baju Zoro",500000],["Baju H&N",250000],["Sweater Uniklooh",175000],["Casing Handphone",50000]]
    var response={}   ;
    var listPurchased = [];
    var sisa = money
    if(memberId =="" || memberId == undefined)
    {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else
    {
        if(sisa>=50000)
        {
            for (let index = 0; index <barang.length; index++) 
            {
                if(money>=barang[index][1])
                {
                    listPurchased.push(barang[index][0])
                    sisa -= barang[index][1]
                }
                
            }
            response.memberId = memberId;
            response.money = money;
            response.listPurchased = listPurchased;
            response.changeMoney = sisa;
            return response
        }else
        {
            return "Mohon maaf, uang tidak cukup"
        }
        
    }
    
  }
//console.log("soal no.2")
//console.log(shoppingTime('1820RzKrnWn08', 2475000));
//console.log(shoppingTime('82Ku8Ma742', 170000));
//console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
//console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
//console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//No.3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var respone = []
    arrPenumpang.forEach(element => 
        {
        var object = {};
        object.penumpang = element[0];
        object.naikDari = element[1];
        object.tujuan = element[2];
        var ongkos = 0;
        for (let index = 0; index < rute.length; index++) 
        {
            if(rute[index] == element[1])
            {
                dari = index
                for (let index2 = index; index2 < rute.length; index2++) {
                    if(rute[index2] == element[2])
                    {
                        ongkos = 2000 * (index2 - index)
                    }
                    
                }
            }
            
        }
        object.bayar = ongkos;
        respone.push(object);
    });
    return respone
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]