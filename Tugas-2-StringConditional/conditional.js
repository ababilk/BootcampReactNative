//if else
var nama = "John"
var peran = "Werewolf"

if (nama == "" && peran == "") {
    console.log("Nama harus diisi!")
}if (nama != "" && peran == "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
}if (nama != "" && peran == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf," + nama)
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
}if (nama != "" && peran == "Guard") {
    console.log("Selamat datang di Dunia Werewolf," + nama)
    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}if (nama != "" && peran == "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf," + nama)
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!" )
}

//switch case
var hari = 1; 
var bulan = 13; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
switch (bulan) {
    case 1:
        bulan = "Januari"
        break;
    case 2:
        bulan = "Februari"
        break;
    case 3:
        bulan = "Maret"
        break;
    case 4:
        bulan = "April"
        break;
    case 5:
        bulan = "Mei"
        break;
    case 6:
        bulan = "Juni"
        break;
    case 7:
        bulan = "Juli"
        break;
    case 8:
        bulan = "Agustus"
        break;
    case 9:
        bulan = "September"
        break;
    case 10:
        bulan = "Oktober"
        break;
    case 11:
        bulan = "November"
        break;
    case 12:
        bulan = "Desember"
        break;          
    default:
        bulan = ""
        break;
}
if (bulan != "") {
    console.log(hari + " " + bulan + " "+ tahun)
}else{
    console.log("bulan harus diisi antara 1-12")
}