//No.1
//const golden = function goldenFunction(){
//   console.log("this is golden!!")
//  }
   
//  golden()

golden = () =>{
    console.log("this is golden!!")
}
golden()

//No.2 yg bener newFunction2
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  //Driver Code 
//newFunction("William", "Imoh").fullName() 

newFunction2=(firstName, lastName)=>{
    return{
        firstName, lastName,
        fullName(){
            console.log(`${firstName} - ${lastName}`)
            return
        }
    }
}

//console.log(newFunction2("William2", "Imoh2").fullName)
newFunction2("William2", "Imoh").fullName()
const firstName3 = 'zell'
const lastName3 ='Liew'
const teamName = 'unaffiliated'
const thestring = `${firstName3} _ ${lastName3},@ ${teamName}`
//console.log(thestring)

//No.3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
//const firstName = newObject.firstName;
//const lastName = newObject.lastName;
//const destination = newObject.destination;
//const occupation = newObject.occupation;
//console.log(firstName, lastName, destination, occupation)

const{firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation)

//No.4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
const combinedES6 = [...west,...east]
//Driver Code
//console.log(combined)
//console.log(combinedES6)

//No.5
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
//console.log(before) 
const after = `Lorem ${view}dolor sit amet, consectetur adipiscong elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
//console.log(after)