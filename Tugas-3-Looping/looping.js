//No. 1 Looping While
var flag = 2;
console.log("LOOPING PERTAMA");
while(flag<=20){
    console.log(flag + " - I love coding")
    flag +=2
}
flag = 20
console.log("LOOPING KEDUA")
while(flag>=2){
    console.log(flag + " - I will become a mobile developer")
    flag -=2
}

console.log("")
//No. 2 Looping menggunakan for
console.log("Looing for")
var iterate = 1
for (let index = 1; index <= 20; index++) {
    if(index % 3 == 0){
        if(index % 2 == 1){
            console.log(index + " - I Love Coding")
        }else{
            console.log(index + " - Berkualitas")
        }
    }else{
        if(index % 2 == 1){
            console.log(index + " - Santai")
        }else{
            console.log(index + " - Berkualitas")
        }
    }
    
}

var pagar = "#";
console.log("")
//No. 3 Membuat persegi panjang
var panjang = 8;
var lebar = 4;

for (let index = 1; index < panjang; index++) {
    pagar +="#"
}
for (let index = 1; index <= lebar; index++) {
    console.log(pagar)
}

pagar = "#";
console.log("")
//No. 4 Membuat tangga
var tangga = 7;
for (let index = 1; index <= tangga; index++){
    console.log(pagar);
    pagar +="#"
}

pagar = "#";
console.log("")
//No. 5 papan catur
var catur = 8;
var putih = " ";
var hitam = "#";
var ganjil = "";
var genap = "";
for (let index = 1; index <= catur; index++) {
    if(index % 2 == 1){
        ganjil += putih;
        genap += hitam;
    }else{
        ganjil += hitam;
        genap += putih
    }
    
}
for (let index = 1; index <= catur; index++) {
    if(index % 2 == 1){
        console.log(ganjil)
    }else{
        console.log(genap)
    }
    
}
